﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHL.Models
{
    public class Software : MasterProperty
    {
        public string Title { get; set; }
        public string SOFTWAREID { get; set; }
        public string SOFTWARETYPE { get; set; }
        public string SOFTWARENAME { get; set; }
        public string LICENCENUMBER { get; set; }
        public string MANUFACTURER { get; set; }
        public string NOOFLICESES { get; set; }
        public string PURCHASEDATE { get; set; }
        public decimal PURCHASEPRICE { get; set; }
        public string PURCHASEDFROM { get; set; }
        public string NEXTRENEWALDATE { get; set; }

    }
}