﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHL.Models
{
    
    public class Category : MasterProperty
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }

    }
}