﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHL.Models
{
    public class Device
    {
        public string SystemName { get; set; }
        public string ASSETNUMBER { get; set; }
        public string ASSETTYPE { get; set; }
        public string DATEOFPURCHASE { get; set; }
        public string WARRENTYBEGIN { get; set; }
        public string WARRENTYEND { get; set; }
        public string MANUFACTURER { get; set; }
        public string MODEL { get; set; }
        public string SERIALNUMBER { get; set; }
        public string PROCESSORTYPE { get; set; }
        public string HDD { get; set; }
        public string RAM { get; set; }
        public string GRAPHICCARD { get; set; }
        public string CDDVDROAM { get; set; }
        public string LOCATION { get; set; }
        public string PURCHASEPRICE { get; set; }
        public string DISPOSEDDATE { get; set; }
        public string ASSETDEPRECIATION { get; set; }
        public string DEPRECIATIONMETHOD { get; set; }
        public string STATUS { get; set; }
        public string Remarks { get; set; }
        
    }
}