﻿using System.Web;
using System.Web.Optimization;

namespace CHL
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/adminlte.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/dataTable").Include(
                       "~/Scripts/jquery.dataTables.min.js",
                       "~/Scripts/dataTables.bootstrap.min.js",
                       "~/Scripts/dataTables.fixedHeader.min.js",
                        "~/Scripts/dataTables.responsive.min.js",
                       "~/Scripts/responsive.bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/appscript").Include(
                        "~/Scripts/moment.min.js",
                        "~/Scripts/bootstrap-datepicker.min.js",
                        "~/Scripts/AppScript/layout.js"
                         ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/ionicons.min.css",
                      "~/Content/jquery-jvectormap.css",
                      "~/Content/AdminLTE.min.css",
                      "~/Content/skin-blue.min.css",
                      "~/Content/loading.css"));

            bundles.Add(new StyleBundle("~/Content/datapicker").Include(
                    "~/Content/bootstrap-datepicker.min.css"
                   ));

          bundles.Add(new StyleBundle("~/Content/dataTableCss").Include(
                     "~/Content/dataTables.bootstrap.min.css",
                     "~/Content/fixedHeader.bootstrap.min.css",
                     "~/Content/responsive.bootstrap.min.css" ));



            BundleTable.EnableOptimizations = true;
        }
    }
}
