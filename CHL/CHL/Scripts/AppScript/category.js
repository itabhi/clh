﻿
$(document).ready(function () {

    var table = $('#tbl_category').DataTable({
        responsive: true,
        processing: true,
        "ajax": "GetAllCategory",
        'ordering': false,
        "columns": [
            { "data": "Name" },
            { "data": "Status" },
            {
                "data": "CreatedDate",
                "mRender": function (data, type, row) {
                    var date = moment(data).format('MMMM Do YYYY, h:mm:ss a');
                    return date;
                }
            },
            {
                "data": "Name",
                "mRender": function (data, type, row) {
                    return '<a href="" name="' + row.Name + '" id="' + row.Id + '"   class="btn btn-info btn-flat btnEdit" data-toggle="modal" data-target="#edit_category_model">Edit</a>' +
                        '<a class="btn btn-info btn-flat btnDelete" style = "margin-left:14px" href = "" data-toggle="modal" data-target="#del_Opener" > Delete</a >';
                }
            },

        ]
    });

    new $.fn.dataTable.FixedHeader(table);

    $('#btn-category').click(function () {
        if (IsValid()) {
            showProgress();
            var $this = $('#frmCategory');
            var frmValues = $this.serialize();
            $.ajax({
                type: $this.attr('method'),
                url: $this.attr('action'),
                data: frmValues
            })
                .done(function () {
                    hideProgress();
                    $('#InputcategoryName').val("")
                    $("#btn_close_category").click();
                })
                .fail(function () {
                    hideProgress();
                  //  $("#btn_close_category").click();
                });
            return false;
        }
        else {
            return false;
        }

    })

    $('#btn_edit_category').click(function () {
        if (IsValidEdit()) {
            showProgress();
            var $this = $('#frmEditCategory');
            var frmValues = $this.serialize();
            $.ajax({
                type: $this.attr('method'),
                url: $this.attr('action'),
                data: frmValues
            })
                .done(function () {
                    hideProgress();
                    $('#InputcategoryEditName').val("")
                    $("#btn-close_editcategory").click();
                })
                .fail(function () {
                    hideProgress();
                    
                });
            return false;
        }
        else {
            return false;
        }

    })

    function IsValid() {
        var IsValid = true;
         
        if ($('#InputcategoryEditName').val() == '') {
            $('#InputcategoryEditName').css('border-color', 'red');
            IsValid = false;
        } else {
            $('#InputcategoryEditName').css('border-color', '');
        }

        return IsValid;
    }
    function IsValidEdit() {
        var IsValid = true;

        if ($('#InputcategoryEditName').val() == '') {
            $('#InputcategoryEditName').css('border-color', 'red');
            IsValid = false;
        } else {
            $('#InputcategoryEditName').css('border-color', '');
        }

        return IsValid;
    }



});
