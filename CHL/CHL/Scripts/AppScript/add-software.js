﻿$(function () {
    $('#PURCHASEDATE,#NEXTRENEWALDATE').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
    })
    //$("input").prop('required', true);

    $('#btn-soft').click(function () {
        if (IsValid()) {
            showProgress();
            var $this = $('#frmsoftware');
            var frmValues = $this.serialize();
            $.ajax({
                type: $this.attr('method'),
                url: $this.attr('action'),
                data: frmValues
            })
                .done(function () {
                    hideProgress();
                    $('#successMesg').text('Submited Successfully').css('color', 'green')
                    $('#frmsoftware').find("input,select,textarea").val("");
                })
                .fail(function () {
                    hideProgress();
                    $('#dangerMesg').text('An error occured').css('color', 'red')
                });
            return false;
        }
        else {
            return false;
        }

    })

    function IsValid() {
        var IsValid = true;
        $('input,select,textarea').each(function () {
            if ($(this).val() == '') {
                $(this).css('border-color', 'red');
                IsValid = false;
            } else {
                $(this).css('border-color', '');
            }
        })
        return IsValid;
    }
});




