﻿
$(document).ready(function () {

    var table = $('#tbl__sub_category').DataTable({
        responsive: true,
        processing: true,
        "ajax": "GetAllCategory",
        'ordering': false,
        "columns": [
            { "data": "Name" },
            { "data": "Status" },
            {
                "data": "CreatedDate",
                "mRender": function (data, type, row) {
                    var date = moment(data).format('MMMM Do YYYY, h:mm:ss a');
                    return date;
                }
            },
            {
                "data": "Name",
                "mRender": function (data, type, row) {
                    return '<a href="" name="' + row.Name + '" id="' + row.Id + '"   class="btn btn-info btn-flat btnEdit" data-toggle="modal" data-target="#edit_sub_category_model">Edit</a>' +
                        '<a class="btn btn-info btn-flat btnDelete" style = "margin-left:14px" href = "" data-toggle="modal" data-target="#del__sub_Opener" > Delete</a >';
                }
            },

        ]
    });

    new $.fn.dataTable.FixedHeader(table);

    $('#btn-subcategory').click(function () {
        if (IsValid()) {
            showProgress();
            var $this = $('#frmsubcategory');
            var frmValues = $this.serialize();
            $.ajax({
                type: $this.attr('method'),
                url: $this.attr('action'),
                data: frmValues
            })
                .done(function () {
                    hideProgress();
                    $('#InputsubcategoryName').val("");
                    $("#btn_close_subcategory").click();
                })
                .fail(function () {
                    hideProgress();

                });
            return false;
        }
        else {
            return false;
        }

    })

    $('#btn_edit_subcategory').click(function () {
        if (IsValidSubcategoryEdit()) {
            showProgress();
            var $this = $('#frmEditSubCategory');
            var frmValues = $this.serialize();
            $.ajax({
                type: $this.attr('method'),
                url: $this.attr('action'),
                data: frmValues
            })
                .done(function () {
                    hideProgress();
                    $('#InputsubcategoryNameEdit').val("");
                    $("#btn_close_editsubcategory").click();
                })
                .fail(function () {
                    hideProgress();

                });
            return false;
        }
        else {
            return false;
        }

    })
    function IsValid() {
        var IsValid = true;
        
        if ($('#InputsubcategoryName').val() == '') {
            $('#InputsubcategoryName').css('border-color', 'red');
                IsValid = false;
            } else {
            $('#InputsubcategoryName').css('border-color', '');
            }
        
        return IsValid;
    }

    function IsValidSubcategoryEdit() {
        var IsValid = true;

        if ($('#InputsubcategoryNameEdit').val() == '') {
            $('#InputsubcategoryNameEdit').css('border-color', 'red');
            IsValid = false;
        } else {
            $('#InputsubcategoryNameEdit').css('border-color', '');
        }

        return IsValid;
    }


     


});
