﻿function showProgress() {
    modal = document.createElement("div");
    modal.className = "modal";
    document.body.appendChild(modal);
    loading = document.getElementsByClassName("loading")[0];
    loading.style.display = "block";
};
function hideProgress() {
    modal = document.createElement("div");
    modal.className = "modal";
    document.body.appendChild(modal);
    loading = document.getElementsByClassName("loading")[0];
    loading.style.display = "none";
};