﻿using CHL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHL.Controllers
{
    public class DeviceCategoryController : Controller
    {
        // GET: DeviceCategory
        [HttpGet]
        public ActionResult Category()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCategory(Category model)
        {
            return Json(true);
        }
        [HttpPost]
        public ActionResult UpdateCategory(Category model)
        {
            return Json(true);
        }
        [HttpGet]
        public ActionResult Subcategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddSubcategory(Subcategory model)
        {
            return Json(true);
        }
        [HttpPost]
        public ActionResult UpdateSubcategory(Subcategory model)
        {
            return Json(true);
        }

        [HttpGet]
        public ActionResult GetAllCategory()
        {

            List<Category> categories = new List<Category>{
                       new Category {Name="Computer", Status="Active",CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                       new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                        new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                         new Category {Name="Computer Accessories", Status="Active",CreatedDate=DateTime.Now },
                          new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                           new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                            new Category {Name="Computer Accessories",Status="Active", CreatedDate=DateTime.Now },
                             new Category {Name="Golu",Status="Active", CreatedDate=DateTime.Now },

                   };
            ;
            var listObj = new PagingModel<List<Category>>()
            {
                data = categories.ToList(),
                draw = 1,
                recordsFiltered = 50,
                recordsTotal = 50
            };

            return Json(listObj, JsonRequestBehavior.AllowGet);
        }




    }
}